import { Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { NoContentComponent } from './no-content';

export const ROUTES: Routes = [
  { path: '',      component: AuthComponent },
  { path: 'cinema', loadChildren: './+cinema#CinemaModule'},
  { path: '**',    component: NoContentComponent }
];
