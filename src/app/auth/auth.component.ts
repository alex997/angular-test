import {
  Component,
  OnInit
} from '@angular/core';
import { NgForm} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
  selector: 'home',  // <home></home>
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
  /**
   * Our list of styles in our component. We may add more to compose many styles together.
   */
  styleUrls: [ './auth.component.css' ],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit {
  /**
   * Set our default values
   */
  /**
   * TypeScript public modifiers
   */
  condition: boolean = true;
  constructor(private router: Router){}
  onSubmit(form: NgForm){
    this.condition = false;
    this.router.navigate(['./cinema']);
}
 

  public ngOnInit() {
    console.log('hello `Home` component');
    /**
     * this.title.getData().subscribe(data => this.data = data);
     */
  }

}
