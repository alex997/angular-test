/**
 * Angular 2 decorators and services
 */
import { Component, ViewEncapsulation } from '@angular/core';
import { environment } from 'environments/environment';
import { NgForm} from '@angular/forms';
import {Router} from '@angular/router';

export const ROOT_SELECTOR = 'app';

/**
 * App Component
 * Top Level Component
 */
@Component({
  selector: ROOT_SELECTOR,
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.css'
  ],
  template: `
  <router-outlet></router-outlet>
`
})
export class AppComponent{
 

}

/**
 * Please review the https://github.com/AngularClass/angular-examples/ repo for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @AngularClass on twitter
 * or our chat on Slack at https://AngularClass.com/slack-join
 */
