import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {DropdownModule} from 'primeng/dropdown';
import { routes } from './producer.routes';
import { ProducerComponent } from './producer.component';
import '../../../../node_modules/primeicons/primeicons.css';
import '../../../../node_modules/primeng/resources/themes/nova-light/theme.css';
import '../../../../node_modules/primeng/resources/primeng.min.css';

console.log('`ChildDetail` bundle loaded asynchronously');

@NgModule({
  declarations: [
    /**
     * Components / Directives/ Pipes
     */
    ProducerComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    DropdownModule,
    AngularFontAwesomeModule,
    RouterModule.forChild(routes),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class ProducerModule {
  public static routes = routes;
}
