import { ProducerComponent } from './producer.component';

export const routes = [
  { path: '', component: ProducerComponent,  pathMatch: 'full' },
];
