import {
  Component,
  OnInit,
} from '@angular/core';
import {SelectItem} from 'primeng/api';
import { NgForm} from '@angular/forms';
import {Router} from '@angular/router';
/**
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

console.log('`AddCinema` component loaded asynchronously');
interface City {
  name: string;
  code: string;
}
@Component({
  templateUrl:"./producer.component.html",
  styleUrls:['./producer.component.css'],
})

export class ProducerComponent{
  array: any[] = [{
    "name": "Bob",
    "age": 45
},{
    "name": "Tom",
    "age": 41
},{
    "name": "Alice",
    "age": 54
},
{
  "name": "Alan",
  "age": 28
},
{
  "name": "Spielberg",
  "age": 71
},
{
  "name": "Christopher",
  "age": 48
}];


}
