import {
  Component,
  OnInit,
} from '@angular/core';
import { HttpClient} from '@angular/common/http';
/**
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

console.log('`Cinema` component loaded asynchronously');

@Component({
  selector: 'books',
  template: `
  <div [ngClass]="{wrapper:true}"> 
  <div [ngClass]="{sidebooks:true}"> 
    <div [ngClass]="{logo:true}"> 
    <div [ngClass]="{logobody:true}"> 
    Logo
    </div>
    </div>
    <a [routerLink]=" ['./']"><fa name="book"></fa></a>
    <a [routerLink]=" ['./add-cinema'] "><fa name="plus"></fa></a>
    <a [routerLink]=" ['./producer'] "><fa name="users"></fa></a>
  </div>
  <div [ngClass]="{books:true}"> 
    <div [ngClass]="{headerbooks:true}">  
     <div [ngClass]="{search:true}">  
      <input type="text" [(ngModel)] = "name" placeholder = " Title">
      <button (click) = "search()">Найти</button>
     </div> 
      <span>
        <a [routerLink]=" ['./add-cinema'] ">
          Add Books
        </a>
      </span>
   </div>
    <div [ngClass] = "{bodybooks:true}">
      <table>
        <thead> 
          <tr>
            <td>Фильм</td>
            <td>Год</td>
            <td>Жанр</td>
            <td>Id</td>
          </tr>
        </thead>
        <tbody> 
          <ng-container *ngIf="response">
            <tr *ngFor="let movies of array">
              <td>{{movies.Title}}</td>
              <td>{{movies.Year}}</td>
              <td>{{movies.Type}}</td>
              <td>{{movies.imdbID}}</td>
            </tr> 
          </ng-container>
        </tbody>
        
      </table>
    </div>
  </div>
  </div>
    
    <router-outlet></router-outlet>
  `,
  styleUrls:['./cinema.component.css'],
})
export class CinemaComponent{
  condition: boolean = false;
  name:string = "";
  response: any;
  array=[];
  constructor(private http: HttpClient){}
  search(){
    this.http.get('http://www.omdbapi.com/?apikey=b5a3bde6&r=json&s='+this.name).subscribe((response)=>{
      this.response = response["Search"];
      this.array = this.response;
      console.log(this.response);
      console.log(this.array);
    })
  }

}
