import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { routes } from './cinema.routes';
import { CinemaComponent } from './cinema.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

console.log('`Books` bundle loaded asynchronously');

@NgModule({
  declarations: [
    /**
     * Components / Directives/ Pipes
     */
    CinemaComponent,
  ],
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
})
export class CinemaModule {
  public static routes = routes;
}
