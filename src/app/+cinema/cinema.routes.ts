import { CinemaComponent } from './cinema.component';

export const routes = [
  { path: '', children: [
    { path: '', component: CinemaComponent },
    { path: 'add-cinema', loadChildren: './+add-cinema#AddCinemaModule' },
    { path: 'producer', loadChildren: './+producer#ProducerModule' }
  ]},
];
