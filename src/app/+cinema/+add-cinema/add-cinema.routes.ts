import { AddCinemaComponent } from './add-cinema.component';

export const routes = [
  { path: '', component: AddCinemaComponent,  pathMatch: 'full' },
];
