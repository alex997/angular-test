import {
  Component,
  OnInit,
} from '@angular/core';
import {SelectItem} from 'primeng/api';
import { NgForm} from '@angular/forms';
import {Router} from '@angular/router';
/**
 * We're loading this component asynchronously
 * We are using some magic with es6-promise-loader that will wrap the module with a Promise
 * see https://github.com/gdi2290/es6-promise-loader for more info
 */

console.log('`AddCinema` component loaded asynchronously');
interface City {
  name: string;
  code: string;
}
@Component({
  templateUrl:"./add-cinema.component.html",
  styleUrls:['./add-cinema.component.css'],
})

export class AddCinemaComponent implements OnInit {


  genre: SelectItem[];
  selectedGenre: string;
  constructor( private router: Router) {
      //SelectItem API with label-value pairs
      //An array of cities
      this.genre = [
        {label: 'Comedy', value: 'Comedy'},
        {label: 'Action', value: 'Action'},
        {label: 'Drama', value: 'Drama'},
        {label: 'Adventure', value: 'Adventure'},
        {label: 'Horror', value: 'Horror'},
    ];
  }

  public ngOnInit() {
    console.log('hello `ChildDetail` component');
  }
 onSubmit(form: NgForm){
  console.log(form.value.login + "  " + form.value.password);
  this.router.navigate(['./cinema']);
  }
  count: number = 0;

  clickEvent() {
        this.count++;
    }
  

}
